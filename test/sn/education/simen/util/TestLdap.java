package sn.education.simen.util;

import java.io.UnsupportedEncodingException;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestLdap {
	
	private static DirContext ctx;
	
	private Logger logger = Logger.getLogger(TestLdap.class);
	
	
	Attributes attrs = new BasicAttributes(true); 
	String userDN = "CN=mndiaye3,OU=Users,OU=EDUCATION,DC=education,DC=sn";

	@Test
	public void testConnection() {
		Assert.assertNotNull(ctx);
	}
	
	@Before
	public void init() {
		ctx = Ldap.getContext();
	}
	
	@After
	public void close() {
		try {
			ctx.close();
		} catch (NamingException e) {
			logger.error("Unable to close the ldap context");
		}
	}
	
	/*@Test
	public void testAddEntry() {
		
		attrs.put("objectClass","user");
		attrs.put("samAccountName","mndiaye1");
		attrs.put("givenname","Malick");
		attrs.put("sn","Ndiaye");
		attrs.put("displayName","Malick Ndiaye");
		String password = "123AZEqsd";
		attrs.put("password", password);
		String newQuotedPassword = "\"" + password + "\"";
		byte[] newUnicodePassword;
		try {
			//newUnicodePassword = newQuotedPassword.getBytes("UTF16-LE");
			//attrs.put("unicodePwd",newUnicodePassword); 
		    int UF_NORMAL_ACCOUNT = 0x0200;
			attrs.put("userAccountControl",Integer.toString(UF_NORMAL_ACCOUNT));  //should expire the password to force the user to change their password at first logon.
			Context result = ctx.createSubcontext(userDN,attrs);
			logger.info("Successfully added ");
		//} catch (UnsupportedEncodingException e) {
			//logger.error("Error with unsupportedEncoding" ,e );
		} catch (NamingException e) {
			logger.error("Error with NamingException" ,e );
		} 
		
	}
	*/
	
	@Test
	public void testAddEntryAsadumar() {
	    Attributes container = new BasicAttributes();

	    // Create the objectclass to add
	    Attribute objClasses = new BasicAttribute("objectClass");
	    objClasses.add("top");
	    objClasses.add("person");
	    objClasses.add("organizationalPerson");
	    objClasses.add("user");

	    String firstName ="Malick";
	    String lastName = "Ndiaye";
	    String domain_name = "education.sn";
	    String organisationUnit = "SIMEN";
	    String password = "123AZEqsd";
	    String username = "mndiaye12";
	    
	    String cnValue = username;
	    Attribute cn = new BasicAttribute("cn", cnValue);
	    Attribute sAMAccountName = new BasicAttribute("sAMAccountName", username);
	    Attribute principalName = new BasicAttribute("userPrincipalName", username + "@" + domain_name);
	    Attribute givenName = new BasicAttribute("givenName", firstName);
	    Attribute sn = new BasicAttribute("sn", lastName);
	    Attribute uid = new BasicAttribute("uid", username);
	    String completeUserName = getUserDN(cnValue, organisationUnit);

	    //some useful constants from lmaccess.h
	    int UF_ACCOUNTENABLE = 0x0001;
	    //int UF_ACCOUNTDISABLE = 0x0002;
	    int UF_PASSWD_NOTREQD = 0x0020;
	    int UF_PASSWD_CANT_CHANGE = 0x0040;
	    int UF_NORMAL_ACCOUNT = 0x0200;
	    int UF_DONT_EXPIRE_PASSWD = 0x10000;
	    //int UF_PASSWORD_EXPIRED = 0x800000;

	    //Note that you need to create the user object before you can set the password. Therefore as the user is created with no
	    //password, user AccountControl must be set to the following otherwise the Win2K3 password filter will return error 53 unwilling to perform.

	    Attribute userCont = new BasicAttribute("userAccountControl",Integer.toString(UF_NORMAL_ACCOUNT + UF_PASSWD_NOTREQD + UF_DONT_EXPIRE_PASSWD + UF_ACCOUNTENABLE ));

	    // Add these to the container
	    container.put(objClasses);
	    container.put(sAMAccountName);
	    container.put(principalName);
	    container.put(cn);
	    container.put(sn);
	    container.put(givenName);
	    container.put(uid);
	    container.put(userCont);

	    try {

		    ctx.createSubcontext(completeUserName, container);
		    //ModificationItem[] mods = new ModificationItem[2];
	
		    //Replace the "unicdodePwd" attribute with a new value Password must be both Unicode and a quoted string
		    //String newQuotedPassword = "\"" + password + "\"";
		    //byte[] newUnicodePassword = newQuotedPassword.getBytes("UTF-16LE");
	
		    //mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("unicodePwd", newUnicodePassword));
		    //mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("userAccountControl",Integer.toString(UF_NORMAL_ACCOUNT + UF_DONT_EXPIRE_PASSWD)));
	
		    //ctx.modifyAttributes(completeUserName, mods);
	
		    logger.info("Create user: "+completeUserName);

	    } catch (Exception e) {
	    	logger.error("Error \n "+e);
	    }
	}

	private String getUserDN(String cnValue, String organisationUnit) {
		return "CN="+cnValue+",OU=Users,OU=EDUCATION,DC=education,DC=sn";
	}
}
