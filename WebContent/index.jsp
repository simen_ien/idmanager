<html ng-app="app">
	<head>
		<title>SIMEN Inscription</title>
		<link href="include/styles.css" rel="stylesheet">
		<!-- Use Bootstrap -->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.min.js"></script>		
		
		<script type="text/javascript" src="include/app.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	
	<body ng-controller="HttpCtrl as app">
  	<div class="container">
		<div class="header">
			<h1 class="custom">SIMEN</h1>
		</div>
		
		<div class="MainBody">
		    <form>
		    	<table>
					<tr>
						<td><button ng-click="addNew()" class="btn btn-primary btn-sm">
						<span class="glyphicon glyphicon-plus"></span> Add New </button></td>
					</tr>
				</table>
		    </form>
		    
			<form id="main">
				<table>
					<tr>
						<td class="display_bold"><label for="actor.name">ID:</label></td>
					</tr>
					<tr>
						<td class="display"><input id="id" type="text" ng-model="actor.id" size="4"></td>
					</tr>
					<tr>
						<td class="display_bold"><label for="name">Name:</label></td>
					</tr>
					<tr>
						<td class="display"><input type="text" ng-model="actor.firstName" size="30"></td>
					</tr>
					<tr>
						<td class="display_bold"><label for="name">Last Name:</label></td>
					</tr>
					<tr>
						<td class="display"><input type="text" ng-model="actor.lastName" size="40"></td>
					</tr>
					<tr>
						<td class="display_bold"><label for="name">Birth Date:</label></td>
					</tr>
					<tr>
						<td class="display"><input type="text" ng-model="actor.dob" size="20"></td>
					</tr>
					<tr>
						<td class="display_bold"><label for="name">Email:</label></td>
					</tr>
					<tr>
						<td class="display"><input type="text" ng-model="actor.email" size="30"></td>
					</tr>
					
					
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
						  <table>						  	
						  	<tr>
						  	  <td><button ng-click="saveActor(actor.id)" class="btn btn-success btn-sm" title="Save actor's details..." ng-disabled="isSaveDisabled">
						  	  <span class="glyphicon glyphicon-plus"></span> S'inscrire </button></td> 
						  	</tr>
						  </table>
						</td>
					</tr>
					
				</table>
			</form>
		</div>

		<div class="footer">SIMEN - Copyright � education.sn</div>
	</div>
	</body>
</html>