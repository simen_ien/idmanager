(function () {
    'use strict';
 
    angular
        .module('wizardApp')
        .component('confirmationComponent', {
            templateUrl:  'app/confirmation/confirmation.html',
            controller: 'ConfirmationController',
            controllerAs: 'vm',
            require: {
                // access to the functionality of the parent component called 'formComponent'
                parent: '^formComponent'
            }
        })
})();