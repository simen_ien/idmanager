(function () {
    'use strict';
 
    angular
        .module('wizardApp')
        .controller('ConfirmationController', ConfirmationController);
 
    ConfirmationController.$inject = [];
 
    function ConfirmationController() {
        var vm = this;
        vm.title = 'Thanks for your registration!';
        vm.formData = {};
        
        vm.$onInit = activate;
 
        ////////////////
 
        function activate() {
            // get data from the parent component
            vm.formData = vm.parent.getData();
            console.log('Confirmation feature loaded!');
        }
    }
})();