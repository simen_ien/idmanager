(function () {
    'use strict';
 
    angular
        .module('wizardApp')
        .controller('ContactController', ContactController);
 
    ContactController.$inject = [];
 
    function ContactController() {
        var vm = this;
        vm.title = 'Your Contact Information?';
        vm.formData = {};
        
        vm.$onInit = activate;
 
        ////////////////
 
        function activate() {
            // get data from the parent component
            vm.formData = vm.parent.getData();
            console.log('Contact feature loaded!');
        }
    }
})();

