(function () {
    'use strict';
 
    angular
        .module('wizardApp')
        .component('contactComponent', {
            templateUrl:  'app/contact/contact.html',
            controller: 'ContactController',
            controllerAs: 'vm',
            require: {
                // access to the functionality of the parent component called 'formComponent'
                parent: '^formComponent'
            },
            bindings: {
                // send a changeset of 'formData' upwards to the parent component called 'formComponent'
                formData: '<'
            }
        })
})();