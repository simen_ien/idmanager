(function () {
    'use strict';
 
    angular
        .module('wizardApp')
        .controller('UserTypeController', UserTypeController);
 
    UserTypeController.$inject = [];
 
    function UserTypeController() {
        var vm = this;
        vm.title = 'Please tell us about yourself.';
        vm.formData = {};
 
        vm.$onInit = activate;
        
        ////////////////
 
        function activate() {
            // get data from the parent component
            vm.formData = vm.parent.getData();
            console.log('User Type feature loaded!');
        }
 
    }
})();