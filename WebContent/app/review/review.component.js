(function () {
    'use strict';
 
    angular
        .module('wizardApp')
        .component('reviewComponent', {
            templateUrl:  'app/review/review.html',
            controller: 'ReviewController',
            controllerAs: 'vm',
            require: {
                // access to the functionality of the parent component called 'formComponent'
                parent: '^formComponent'
            },
            bindings: {
                // send a changeset of 'formData' upwards to the parent component called 'formComponent'
                formData: '<'
            }
        })
})();