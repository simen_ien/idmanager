(function () {
    'use strict';
 
    angular
        .module('wizardApp')
        .controller('ReviewController', ReviewController);
 
    ReviewController.$inject = [];
 
    function ReviewController() {
        var vm = this;
        vm.title = 'Could you review your entries?';
        vm.formData = {};
        
        vm.$onInit = activate;
 
        ////////////////
 
        function activate() {
            // get data from the parent component
            vm.formData = vm.parent.getData();
            console.log('Review feature loaded!');
        }
    }
})();

