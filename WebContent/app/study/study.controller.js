(function () {
    'use strict';
 
    angular
        .module('wizardApp')
        .controller('StudyController', StudyController);
 
    StudyController.$inject = [];
 
    function StudyController() {
        var vm = this;
        vm.title = 'What do you study?';
        vm.formData = {};
        
        vm.$onInit = activate;
 
        ////////////////
 
        function activate() {
            // get data from the parent component
            vm.formData = vm.parent.getData();
            console.log('Study feature loaded!');
        }
    }
})();

