(function() {
    'use strict';
 
    // Creating our angular app and inject ui-router 
    // =============================================================================
    var app = angular.module('wizardApp', ['ui.router'])
 
    // Configuring our states 
    // =============================================================================
    app.config(['$stateProvider', '$urlRouterProvider',
 
        function($stateProvider, $urlRouterProvider) {
 
            // For any unmatched url, redirect to /wizard/personal
            $urlRouterProvider.otherwise('/form/userType');
    
            $stateProvider
                // PARENT STATE: form state
                .state('form', {
                    url: '/form',
                    component: 'formComponent'
                })
        
                // NESTED STATES: child states of 'form' state 
                // URL will become '/form/userType'
                .state('form.userType', {
                    url: '/userType',
                    component: 'userTypeComponent'
                })
                
                 // URL will become '/form/identification'
                .state('form.identification', {
                    url: '/identification',
                    component: 'identificationComponent'
                })
                
                // URL will become '/form/identification'
                .state('form.contact', {
                    url: '/contact',
                    component: 'contactComponent'
                })
                
                // URL will become '/form/filiation'
                .state('form.filiation', {
                    url: '/filiation',
                    component: 'filiationComponent'
                })
                
                // URL will become '/form/study'
                .state('form.study', {
                    url: '/study',
                    component: 'studyComponent'
                })
        
                // URL will become /form/work
                .state('form.work', {
                    url: '/work',
                    component: 'workComponent'
                })
        
                // URL will become /form/review
                .state('form.review', {
                    url: '/review',
                    component: 'reviewComponent'
                })
 
                // URL will become /form/confirmation
                .state('form.confirmation', {
                    url: '/confirmation',
                    component: 'confirmationComponent'
                })
        }
    ]);
       
})();