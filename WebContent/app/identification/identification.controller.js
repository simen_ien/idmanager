(function () {
    'use strict';
 
    angular
        .module('wizardApp')
        .controller('IdentificationController', IdentificationController);
 
    IdentificationController.$inject = [];
 
    function IdentificationController() {
        var vm = this;
        vm.title = 'Present yourself?';
        vm.formData = {};
        
        vm.$onInit = activate;
 
        ////////////////
 
        function activate() {
            // get data from the parent component
            vm.formData = vm.parent.getData();
            console.log('Identification feature loaded!');
        }
    }
})();

