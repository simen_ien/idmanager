(function () {
    'use strict';
 
    angular
        .module('wizardApp')
        .controller('FiliationController', FiliationController);
 
    FiliationController.$inject = [];
 
    function FiliationController() {
        var vm = this;
        vm.title = 'What about your filiation?';
        vm.formData = {};
        
        vm.$onInit = activate;
 
        ////////////////
 
        function activate() {
            // get data from the parent component
            vm.formData = vm.parent.getData();
            console.log('Filiation feature loaded!');
        }
    }
})();

