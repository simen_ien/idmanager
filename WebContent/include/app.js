(function() {
	
	var app = angular.module("app", []);

	
	app.controller("HttpCtrl", function($scope, $http) {
		$scope.navTitle = 'All Stars';
		$scope.operation="";
		$scope.isSaveDisabled = true;
		$scope.isDeleteDisabled = true;
		
		
		$scope.addNew = function(element) {
			$scope.operation="create";
			$scope.clearForm();
			main.id.focus();
			$scope.isSaveDisabled = false;
			$scope.isDeleteDisabled = true;
		}
		
		$scope.saveActor = function(id) {
			$scope.jsonObj = angular.toJson($scope.actor, false);
			console.log("[update] data: " + $scope.jsonObj);

			if ($scope.operation == "create") {
				var response = $http.post('/IDManager/rest/user/adduser', $scope.jsonObj);
				response.success(function(data, status) {
					console.log("Inside create operation..." + angular.toJson(data, false) + ", status=" + status);
					$scope.resetForm();
			    });
				
				response.error(function(data, status) {
					alert("AJAX failed to get data, status=" + status, scope);
				})	
			}
		};
		
		$scope.resetForm = function(name) {
			//var app = this;
			$scope.operation="";
			$scope.clearForm();
			$scope.isSaveDisabled = true;
			$scope.isDeleteDisabled = true;
			$scope.navTitle = 'All Stars';
			
		};
	});	
})();