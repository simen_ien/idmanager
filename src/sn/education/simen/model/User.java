package sn.education.simen.model;

import java.util.Date;

import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;


public class User {
	
	
	private String firstName;
	private String lastName;
	private Date dob;
	
	//TODO to replace with enum
	private String sex;
	
	// TODO to replace with enum
	private String language;
	
	private String email;
	private String secondaryEmail;
	
	
	//TODO to create a contact class
	private String address;
	private String city;
	//private String state;
	//private String zipCode;
	private String phone;
	private String secondaryPhone;
	
	private String contactFirstName;
	private String contactLastName;
	private String contactPhone;
	
	private boolean active;
	
	private boolean valid;
	
	//TODO to replace by enum 
	private String type;

		String id;
	   String dn;
	  /* Attributes myAttrs = new BasicAttributes(true);
	   Attribute oc = new BasicAttribute("objectclass");
	   Attribute ouSet = new BasicAttribute("ou");*/
	  /* 
	   String cnValue = userName;
	   Attribute cn = new BasicAttribute(“cn”, cnValue);
	   Attribute sAMAccountName = new BasicAttribute(“sAMAccountName”, userName);
	   Attribute principalName = new BasicAttribute(“userPrincipalName”, userName + “@” + DOMAIN_NAME);
	   Attribute givenName = new BasicAttribute(“givenName”, firstName);
	   Attribute sn = new BasicAttribute(“sn”, lastName);
	   Attribute uid = new BasicAttribute(“uid”, userName);
	   String completeUserName = getUserDN(cnValue, organisationUnit);*/
	
	   public User() {
		   
	   }

	/*public User(String dn, String uid, String lastname, String sn, String ou) {
		
		  this.dn=dn;
	      id = uid;

	      objClasses.add(“top”);
	      objClasses.add(“person”);
	      objClasses.add(“organizationalPerson”);
	      objClasses.add(“user”);
	     
	      ouSet.add("People");
	      ouSet.add(ou);
	      String cn = lastname+" "+sn;
	      myAttrs.put(oc);
	      myAttrs.put(ouSet);
	      myAttrs.put("uid",uid);
	      myAttrs.put("cn",cn);
	      myAttrs.put("sn",sn);
	      myAttrs.put("givenname",lastname);
	      myAttrs.put("mail",mail);
	      myAttrs.put("telephonenumber",tel);
	      myAttrs.put("facsimilietelephonenumber",fax);
	}
*/
	@JsonProperty(value = "first-name")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@JsonProperty(value = "last-name")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@JsonProperty(value = "language")
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	@JsonProperty(value = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty(value = "secondary-email")
	public String getSecondaryEmail() {
		return secondaryEmail;
	}

	public void setSecondaryEmail(String secondaryEmail) {
		this.secondaryEmail = secondaryEmail;
	}

	@JsonProperty(value = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty(value = "city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@JsonProperty(value = "phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@JsonProperty(value = "secondary-phone")
	public String getSecondaryPhone() {
		return secondaryPhone;
	}

	public void setSecondaryPhone(String secondaryPhone) {
		this.secondaryPhone = secondaryPhone;
	}

	@JsonProperty(value = "contact-first-name")
	public String getContactFirstName() {
		return contactFirstName;
	}
	
	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	@JsonProperty(value = "contact-last-name")
	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	@JsonProperty(value = "contact-phone")
	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	@JsonProperty(value = "is-active")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@JsonProperty(value = "is-valid")
	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	@JsonProperty(value = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", dob=" + dob + ", sex=" + sex
				+ ", language=" + language + ", email=" + email + ", secondaryEmail=" + secondaryEmail + ", address="
				+ address + ", city=" + city + ", phone=" + phone
				+ ", secondaryPhone=" + secondaryPhone + ", contactFirstName=" + contactFirstName + ", contactLastName="
				+ contactLastName + ", contactPhone=" + contactPhone + ", active=" + active + ", valid=" + valid
				+ ", type=" + type + "]";
	}
	
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDn() {
		return dn;
	}

	public void setDn(String dn) {
		this.dn = dn;
	}

	/*public Attributes getMyAttrs() {
		return myAttrs;
	}

	public void setMyAttrs(Attributes myAttrs) {
		this.myAttrs = myAttrs;
	}

	public Attribute getOc() {
		return oc;
	}

	public void setOc(Attribute oc) {
		this.oc = oc;
	}

	public Attribute getOuSet() {
		return ouSet;
	}

	public void setOuSet(Attribute ouSet) {
		this.ouSet = ouSet;
	}*/
	
	//TODO Create a filiation class
	
	//TODO create a scholarship class
	
	//TODO create a employment class
	
	//TODO create a degree class
	
	//TODO create a role class
	
	//TODO create a function class
	
	
	
	
	
	
	

}
