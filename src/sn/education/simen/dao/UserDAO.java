/**
 * 
 */
package sn.education.simen.dao;

import javax.ws.rs.core.Response;

import sn.education.simen.model.User;



/**
 * @author mndiaye
 *
 */
public interface UserDAO {

	public Response createUser(User user);
	
}
