/**
 * 
 */
package sn.education.simen.util;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import javax.naming.*;
import javax.naming.directory.*;

public class Ldap {
	
	private static Hashtable<String, String> environment = new Hashtable<String, String>();
	private static DirContext context = null;
	
	
	private static Logger logger = Logger.getLogger(Ldap.class);
	
	private static String ldapConnectionUrl = "LDAP://52.179.14.193:389";//"LDAP://52.179.14.193:389";//"LDAPS://52.179.14.193:636";
	//private static String ldapBaseDN = "DC=education,DC=sn";
	//private static String ldapUserDN = "OU=Users,OU=EDUCATION,DC=education,DC=sn";
	private static String ldapBinDN = "CN=localadmin,CN=Users,DC=education,DC=sn";
	private static String ldapCred = "Adm1nl0cal!!";
	

	static {
		logger.info("Inside ldap() static method... ");
		environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		//environment.put(Context.SECURITY_PROTOCOL, "ssl");
	    environment.put(Context.PROVIDER_URL, ldapConnectionUrl);
	    environment.put(Context.SECURITY_AUTHENTICATION, "simple");
	    environment.put(Context.SECURITY_PRINCIPAL, ldapBinDN);
	    environment.put(Context.SECURITY_CREDENTIALS, ldapCred);
	    environment.put("com.sun.jndi.ldap.connect.timeout", "1000");
	    
		try {
			context = new InitialDirContext(environment);
			logger.info("Connection to ldap successfully");
		} catch (AuthenticationNotSupportedException exception) {
			logger.error("The authentication is not supported by the server \n"+exception);
		} catch (AuthenticationException exception) {
	        logger.error("Incorrect password or username \n"+exception);
	    } catch (NamingException exception) {
	    	logger.error("Error when trying to create the context \n"+exception);
	    }
	}
	
	public static DirContext getContext() {
		return context;
	}
	
	
	
	
	
	
	

}
