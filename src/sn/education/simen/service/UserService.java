package sn.education.simen.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import sn.education.simen.dao.UserDAO;
import sn.education.simen.dao.impl.UserDAOImpl;
import sn.education.simen.model.User;
/**
 * REST Service API
 * @author mndiaye
 *
 */
@Path("/user")
public class UserService {

	private Logger logger = Logger.getLogger(UserService.class);	

	
	@POST
	@Path("adduser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createCustomer(User user) {

		UserDAO daoImpl = new UserDAOImpl();
		logger.info("Inside createUser...");
		
		Response resp = daoImpl.createUser(user);
		return resp;
	}
	
}
